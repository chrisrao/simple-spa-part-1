docker rm $(docker ps -aq --filter="name=simple-spa-ui-1")
docker build --tag=simple-spa-ui-1 ./ui
docker run --name=simple-spa-ui-1 simple-spa-ui-1
docker cp simple-spa-ui-1:/content/dist ./api/ui-dist
docker build --tag=simple-spa-api-1 ./api
docker run -p 5000:5000 simple-spa-api-1