const path = require('path');
const TerserPlugin = require('terser-webpack-plugin');
const webpack = require('webpack');

const distPath = path.join(__dirname, 'dist');
const srcPath = path.join(__dirname, 'src');

module.exports = (env, argv) => {
  const mode = argv ? argv.mode : 'production';
  const config = {
    entry: ['./src/index.js'],
    mode,
    output: {
      filename: 'bundle.js',
      path: distPath,
    },
    optimization: {
      minimizer: [new TerserPlugin({
        parallel: true,
        terserOptions: {
          ecma: 6,
        },
      })],
    },
    resolve: {
      extensions: ['*', '.js'],
      modules: ['node_modules']
    },
  };
  return config;
};
